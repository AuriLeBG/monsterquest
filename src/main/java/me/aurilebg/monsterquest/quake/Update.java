package me.aurilebg.monsterquest.quake;

import org.bukkit.Bukkit;
import org.bukkit.Particle;
import org.bukkit.entity.Projectile;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.UUID;

public class Update extends BukkitRunnable {
    Projectile proj;
    Vector vecteurM;
    Vector vecteur;
    int compteur;
    public Update(Projectile proj){
        this.proj = proj;
        vecteur = proj.getVelocity();
        if(Math.abs(vecteur.getX()) < Math.abs(vecteur.getZ()) ){
            vecteurM = (new Vector(1, 1, 0.9));
        }
        else{
            vecteurM = (new Vector(0.9, 1, 1));
        }
        compteur = 100;
    }

    @Override
    public void run() {

        if(compteur > 0){
            if(proj.getChunk().isLoaded()){
                vecteur = vecteur.multiply(vecteurM);
                proj.setVelocity(vecteur);
                proj.getWorld().spawnParticle(Particle.COMPOSTER, proj.getLocation(), 3);
                if(proj.getLocation().getBlock() != null){
                    proj.teleportAsync(proj.getLocation().add(proj.getVelocity()));
                }
            }


            compteur --;
        }
        else{
            if(proj.getChunk().isLoaded()){
                proj.remove();
            }
            cancel();
        }
    }
}
