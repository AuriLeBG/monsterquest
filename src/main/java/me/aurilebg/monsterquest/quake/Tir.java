package me.aurilebg.monsterquest.quake;

import me.aurilebg.monsterquest.MonsterQuest;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Projectile;
import org.bukkit.util.Vector;

public class Tir {
    public Tir(Location location, MonsterQuest main){
        Projectile proj = (Projectile) location.getWorld().spawnEntity(location.add(0, 1, 0), EntityType.ARROW);
        proj.setVelocity(location.getDirection().multiply(2));
        proj.setGravity(false);
        Update start = new Update(proj);
        start.runTaskTimer(main, 0, 1);
    }
}
