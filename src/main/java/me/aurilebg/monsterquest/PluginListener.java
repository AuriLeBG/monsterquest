package me.aurilebg.monsterquest;

import me.aurilebg.monsterquest.quake.Tir;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityAirChangeEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class PluginListener implements Listener {
    private MonsterQuest main;
    public PluginListener(MonsterQuest main){
        this.main = main;
    }


    @EventHandler
    public void onClick(PlayerInteractEvent event){
        ItemStack item = event.getItem();
        Player player = event.getPlayer();

        if(item == null){
            return;
        }

        if(item.isSimilar(new ItemStack(Material.DIAMOND_HOE, 1))){
            new Tir(player.getLocation(), main);
        }
    }



    @EventHandler
    public void testCollision(ProjectileHitEvent event){
        Entity entity = event.getEntity();
        Entity hitEntity = event.getHitEntity();

        if(entity instanceof Arrow){

            if(hitEntity instanceof LivingEntity){
                LivingEntity hitMonster = (LivingEntity) hitEntity;
                hitMonster.damage(100);
                event.setCancelled(true);
            }
        }
    }
}
