package me.aurilebg.monsterquest;

import org.bukkit.plugin.java.JavaPlugin;

public final class MonsterQuest extends JavaPlugin {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new PluginListener(this), this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
